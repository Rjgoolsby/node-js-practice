/*
https://github.com/jperkin/node-rpio
*/
var rpio = require('rpio');
var http = require('http');
rpio.init({
    mapping: 'gpio'
});

var server = http.createServer(function (request, response) {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Hello World\n");
});

server.listen(8080);

// console.log("Server running at http://127.0.0.1:8080/");


var blueLight = 24;
var greenLight = 23;
var redLight = 18;
var btn = 25;
var valve = 5;
var sensor = 4;

/*
 * Set the initial state to low.  The state is set prior to the pin becoming
 * active, so is safe for devices which require a stable setup.
 */
rpio.open(blueLight, rpio.OUTPUT, rpio.LOW);
rpio.open(btn, rpio.INPUT, rpio.PULL_UP);



/*
 * This callback will be called every time a configured event is detected on
 * the pin.  The argument is the pin that triggered the event, so you can use
 * the same callback for multiple pins.
 */
function pollcb(cbpin)
{
	/*
	 * It cannot be guaranteed that the current value of the pin is the
	 * same that triggered the event, so the best we can do is notify the
	 * user that an event happened and print what the value is currently
	 * set to.
	 *
	 * Unless you are pressing the button faster than 1ms (the default
	 * setInterval() loop which polls for events) this shouldn't be a
	 * problem.
	 */
    var level = rpio.read(cbpin) ? rpio.LOW : rpio.HIGH;
	var state = rpio.read(cbpin) ? 'released' : 'pressed';
	console.log('Button event on P%d (button currently %s)', cbpin, state);

    rpio.write(blueLight,level);

	/*
	 * By default this program will run forever.  If you want to cancel the
	 * poll after the first event and end the program, uncomment this line.
	 */
	// rpio.poll(cbpin, null);
}

rpio.poll(btn, pollcb);
/*
 * The sleep functions block, but rarely in these simple programs does one care
 * about that.  Use a setInterval()/setTimeout() loop instead if it matters.
 */
/*for (var i = 0; i < 5; i++) {
        // On for 1 second
        rpio.write(blueLight, rpio.HIGH);
        rpio.sleep(1);

        // Off for half a second (500ms)
        rpio.write(blueLight, rpio.LOW);
        rpio.msleep(500);
}*/
