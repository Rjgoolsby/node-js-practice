/*
https://github.com/jperkin/node-rpio
https://www.npmjs.com/package/ds18x20
*/
var rpio = require('rpio');
var http = require('http');
var sensor = require('ds18x20');
var W1Temp = require('w1temp');
rpio.init({
    mapping: 'gpio'
});

var server = http.createServer(function (request, response) {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Hello World\n");
});

server.listen(8080);

// console.log("Server running at http://127.0.0.1:8080/");

var mashTemp = 75;
var probes = 4;

W1Temp.getSensorsUids().then(function (sensorsUids) {
    for (var i = 0; i < sensorsUids.length; i++) {
        W1Temp.getSensor(sensorsUids[i]).then(function (sensor) {
            sensor.on('change', function (temp) {
                //Turn on relay if to cold
                handleRelay(convertToF(temp));
                var fileName = sensor.file;
                var sensorName = fileName.replace('/sys/bus/w1/devices/', '').replace('/w1_slave', '');
                console.log(sensorName + ' temp changed:'+ convertToF(temp)+ '°F');
            });
        });
    }
});

function convertToF(temp) {
    var nTemp = (parseFloat(temp) * (9 / 5)) + 32;
    return  parseFloat(nTemp.toFixed(1));
}
function handleRelay(temp){
    console.log('temp is ' + temp + ' mash temp is set at ' + mashTemp);
    if(temp < mashTemp){
        console.log('heater off');
    }else{
        console.log('heater on');
    }
}
