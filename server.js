/*
https://github.com/jperkin/node-rpio
https://www.npmjs.com/package/ds18x20
*/
var rpio = require('rpio');
var http = require('http');
var sensor = require('ds18x20');
var W1Temp = require('w1temp');
rpio.init({
    mapping: 'gpio'
});

var server = http.createServer(function (request, response) {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Hello World\n");
});

server.listen(8080);

// console.log("Server running at http://127.0.0.1:8080/");

var mashTemp = 75;
var blueLight = 24;
var greenLight = 23;
var redLight = 17;
var btn = 25;
var valve = 5;
var probes = 4;
var relay1 = 16;

/*
 * Set the initial state to low.  The state is set prior to the pin becoming
 * active, so is safe for devices which require a stable setup.
 */
rpio.open(blueLight, rpio.OUTPUT, rpio.LOW);
rpio.open(greenLight, rpio.OUTPUT, rpio.LOW);
rpio.open(redLight, rpio.OUTPUT, rpio.LOW);
rpio.open(relay1, rpio.OUTPUT, rpio.LOW);

/*
 * Set Pull up resistor for Btn and Valve
 *
*/
rpio.open(btn, rpio.INPUT, rpio.PULL_UP);
rpio.open(valve, rpio.INPUT, rpio.PULL_UP);

W1Temp.getSensorsUids().then(function (sensorsUids) {
    for (var i = 0; i < sensorsUids.length; i++) {
        W1Temp.getSensor(sensorsUids[i]).then(function (sensor) {
            sensor.on('change', function (temp) {
                //Turn on relay if to cold
                handleRelay(convertToF(temp));
                var fileName = sensor.file;
                var sensorName = fileName.replace('/sys/bus/w1/devices/', '').replace('/w1_slave', '');
                console.log(sensorName + ' temp changed:'+ convertToF(temp)+ '°F');
            });
        });
    }
});

function convertToF(temp) {
    var nTemp = (parseFloat(temp) * (9 / 5)) + 32;
    return  parseFloat(nTemp.toFixed(1));
}
function handleRelay(temp){
    pin = relay1; // relay1
        // rpio.write(relay1, rpio.LOW);
    console.log('temp is ' + temp + ' mash temp is set at ' + mashTemp);
    if(temp < mashTemp){redLight
        rpio.write(pin, rpio.HIGH);
    }else{
        rpio.write(pin, rpio.LOW);
    }
}


/*
 * This callback will be called every time a configured event is detected on
 * the pin.  The argument is the pin that triggered the event, so you can use
 * the same callback for multiple pins.
 */
rpio.poll(btn, function(pin){
    var level = rpio.read(pin) ? rpio.LOW : rpio.HIGH;
	var state = (level == rpio.LOW) ? 'button released' : 'button pressed';
	console.log('Button event on P%d (button currently %s)', pin, state);
    rpio.write(blueLight,level);
    rpio.write(relay1,level);
});

rpio.poll(valve, function(pin){
    var level = rpio.read(pin) ? rpio.HIGH : rpio.LOW;
	var state = (level == rpio.HIGH) ? 'Tank Full' : 'Tank Empty';
	console.log('Valve event on P%d (Valve currently %s)', pin, state);
    rpio.write(redLight,level);
});

/*
Valve Poll
*/
//Global value to store Valve state
/*valveState = rpio.LOW;
var valvePoll = setInterval(function(){
    var pin = 5;
    var newValveState = rpio.read(pin) ? rpio.HIGH : rpio.LOW;
    if(valveState != newValveState){
        valveState = newValveState;
        var state = (valveState == rpio.HIGH) ? 'Tank Full' : 'Tank Empty';
    	console.log('Valve event on P%d (Valve currently %s)', pin, state);
        rpio.write(redLight,valveState);
    }
}, 10000);*/

/*
 * Blink Lights to let us know that we're up and running
 */

var timesRun = 0;
var interval = setInterval(function(){
    timesRun += 1;
    if(timesRun === 3){
        clearInterval(interval);
        console.log('Application ready');
    }
    // On for 1 second
    rpio.write(blueLight, rpio.HIGH);
    rpio.write(greenLight, rpio.HIGH);
    rpio.write(redLight, rpio.HIGH);
    rpio.sleep(1);

    // Off for half a second (500ms)
    rpio.write(blueLight, rpio.LOW);
    rpio.write(greenLight, rpio.LOW);
    rpio.write(redLight, rpio.LOW);
    // rpio.msleep(500);
},500);
